package dam.android.alejandror.u4t6contacts;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private MyContacts myContacts;


    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textView;


        public MyViewHolder(TextView view) {

            // TODO Ex1
            super(view);
            this.textView = view;

        }

        public void bind(String contactData) {

            this.textView.setText(contactData);


        }

    }

    MyAdapter(MyContacts myContacts) {
        this.myContacts = myContacts;
    }


    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        TextView v =  (TextView) LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);

        return new MyViewHolder(v);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        viewHolder.bind(myContacts.getContactData(position));
    }

    @Override
    public int getItemCount() {
        return myContacts.getCount();
    }
}

package dam.android.alejandror.u4t6contacts;

public class ContactItem {

    public String idImagen;
    public String id;
    public String numero;
    public String nombre;

    public ContactItem(String idImagen, String id, String numero, String nombre) {
        this.idImagen = idImagen;
        this.id = id;
        this.numero = numero;
        this.nombre = nombre;
    }

    public String getIdImagen() {
        return idImagen;
    }

    public void setIdImagen(String idImagen) {
        this.idImagen = idImagen;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}

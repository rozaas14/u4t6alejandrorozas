package dam.android.alejandror.u4t6contacts;

public class ContactItem {

    // TODO Ex2: Ahora tenemos todos los items disponibles para hacerles getter
    public String idImagen;
    public String id;
    public String numero;
    public String nombre;
    public String raw;
    public String type;
    public String lookup;

    public ContactItem(String idImagen, String id, String numero, String nombre, String raw, String type, String lookup) {
        this.idImagen = idImagen;
        this.id = id;
        this.numero = numero;
        this.nombre = nombre;
        this.raw = raw;
        this.type = type;
        this.lookup = lookup;
    }

    // TODO Ex2: Todos los getters de todos los atributos
    public String getIdImagen() {
        return idImagen;
    }

    public String getId() {
        return id;
    }

    public String getNumero() {
        return numero;
    }

    public String getNombre() {
        return nombre;
    }

    public String getRaw() {
        return raw;
    }

    public String getType() {
        return type;
    }

    public String getLookup() {
        return lookup;
    }
}

package dam.android.alejandror.u4t6contacts;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements MyAdapter.OnItemClickListener {

    MyContacts myContacts;
    RecyclerView recyclerView;
    TextView datos;

    private static String[] PERMISSIONS_CONTACTS = {Manifest.permission.READ_CONTACTS};

    private static final int REQUEST_CONTACTS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();

        if (checkPermissions()) {
            setListAdapter();
        }

    }

    private void setUI() {

        recyclerView = findViewById(R.id.recyclerViewContacts);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));

        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));

    }

    private void setListAdapter() {
        myContacts = new MyContacts(this);

        recyclerView.setAdapter(new MyAdapter(myContacts, this));

        if (myContacts.getCount() > 0) {

            findViewById(R.id.tvEmptyList).setVisibility(View.INVISIBLE);
        }
    }

    private boolean checkPermissions() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, MainActivity.PERMISSIONS_CONTACTS, MainActivity.REQUEST_CONTACTS);

            return false;
        } else {

            return true;
        }
    }


    // TODO Ex2: Metodo llamado a la hora de hacer click en un item
    public void onItemClick(ContactItem item) {

        datos = findViewById(R.id.tvDatos);

        // TODO EX2: A la hora de hacer click dependiendo del tipo de telefono nos mostrara un tipo o otro
        if (item.getType() == "1") {

            datos.setText(item.getNombre() + ", " +item.getId() + ", " + item.getNumero() +", "+ item.getRaw()
                    + ", HOME " + item.getLookup());

        } else if (item.getType() == "2") {
            datos.setText(item.getNombre() + ", " +item.getId() + ", " + item.getNumero() +", "+ item.getRaw()
                    + ", WORK " + item.getLookup());

        } else if (item.getType() == "3") {
            datos.setText(item.getNombre() + ", " +item.getId() + ", " + item.getNumero() +", "+ item.getRaw()
                    + ", MOBILE " + item.getLookup());

        } else {

            datos.setText(item.getNombre() + ", " +item.getId() + ", " + item.getNumero() +", "+ item.getRaw()
                    + ", OTHER " + item.getLookup());
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {


        if (requestCode == REQUEST_CONTACTS) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                setListAdapter();
            } else {

                Toast.makeText(this, getString(R.string.contacts_read_right_required), Toast.LENGTH_LONG).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        }
    }
}



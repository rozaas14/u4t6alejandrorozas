package dam.android.alejandror.u4t6contacts;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    // TODO Ex2: Interface para hacer una accion a la hora de hacer click
    public interface OnItemClickListener {
        void onItemClick(ContactItem item);
    }

    private MyContacts myContacts;

    // TODO Ex2: Variable para llamar al onclick
    private OnItemClickListener listener;

    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tfId;
        TextView tfTlf;
        TextView tfName;
        ImageView imImage;
        ConstraintLayout cLayout;

        public MyViewHolder(View view) {

            super(view);
            this.tfId = view.findViewById(R.id.id);
            this.tfTlf = view.findViewById(R.id.tlf);
            this.tfName = view.findViewById(R.id.name);
            this.imImage = view.findViewById(R.id.image);
            this.cLayout = view.findViewById(R.id.contact);
        }

        public void bind(ContactItem data, OnItemClickListener listener) {

            this.tfId.setText(data.getId());
            this.tfTlf.setText(data.getNumero());
            this.tfName.setText(data.getNombre());
            if (data.getIdImagen() != null)
                this.imImage.setImageURI(Uri.parse(data.getIdImagen()));

            // TODO Ex2: Le asignamos al contacto que cuando sea clicado haga cierta accion
            this.cLayout.setOnClickListener(v -> listener.onItemClick(data));
        }

    }
    MyAdapter(MyContacts myContacts, OnItemClickListener listener) {
        this.myContacts = myContacts;
        this.listener = listener;
    }


    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card_layout, parent, false);

        return new MyViewHolder(v);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        viewHolder.bind(myContacts.getContactData(position), listener);
    }

    @Override
    public int getItemCount() {
        return myContacts.getCount();
    }
}

package dam.android.alejandror.u4t6contacts;

public class ContactItem {

    // TODO Ex1.3: Clase ContactItem necesaria para crear los contactos que serán anyadidos a la lista
    public String idImagen;
    public String id;
    public String numero;
    public String nombre;

    public ContactItem(String idImagen, String id, String numero, String nombre) {
        this.idImagen = idImagen;
        this.id = id;
        this.numero = numero;
        this.nombre = nombre;
    }

    public String getIdImagen() {
        return idImagen;
    }

    public String getId() {
        return id;
    }

    public String getNumero() {
        return numero;
    }

    public String getNombre() {
        return nombre;
    }
}

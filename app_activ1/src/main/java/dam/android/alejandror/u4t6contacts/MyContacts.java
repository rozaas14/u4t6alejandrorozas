package dam.android.alejandror.u4t6contacts;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;

import java.util.ArrayList;

public class MyContacts {

    private ArrayList<ContactItem> myDataSet;
    private Context context;

    public MyContacts(Context context) {

        this.context = context;
        this.myDataSet = getContacts();

    }


    private ArrayList<ContactItem> getContacts() {

        ArrayList<ContactItem> contactsList = new ArrayList<>();

        ContentResolver contentResolver = context.getContentResolver();

        // TODO EX1.1: Cambiamos la consulta de los datos que recibimos
        String[] projection = new String[]{ContactsContract.Data._ID,
                ContactsContract.Data.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.Data.LOOKUP_KEY,
                ContactsContract.Contacts.PHOTO_THUMBNAIL_URI,
                ContactsContract.Data.RAW_CONTACT_ID,
                ContactsContract.CommonDataKinds.Phone.TYPE};

        String selectionFilter = ContactsContract.Data.MIMETYPE + "='" +
                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE + "' AND " +
                ContactsContract.CommonDataKinds.Phone.NUMBER + " IS NOT NULL";

        Cursor contactsCursor = contentResolver.query(ContactsContract.Data.CONTENT_URI,
                projection,
                selectionFilter,
                null,
                ContactsContract.Data.DISPLAY_NAME + " ASC");

        if (contactsCursor != null) {

            // TODO Ex1.1: Obtenemos todas las variables necesarias para la creacion de un contacto
            int nameIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.DISPLAY_NAME);
            int numberIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int photoIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Contacts.PHOTO_THUMBNAIL_URI);
            int rawIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.RAW_CONTACT_ID);
            int typeIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.TYPE);
            int lookupIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.LOOKUP_KEY);
            int idInex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data._ID);

            while (contactsCursor.moveToNext()) {

                String name = contactsCursor.getString(nameIndex);
                String number = contactsCursor.getString(numberIndex);
                String photo = contactsCursor.getString(photoIndex);
                String raw = contactsCursor.getString(rawIndex);
                String type = contactsCursor.getString(typeIndex);
                String lookup = contactsCursor.getString(lookupIndex);
                String id = contactsCursor.getString(idInex);

                // TODO Ex1.1 Creamos el contacto que se mostrara en nuestra app
                ContactItem item = new ContactItem(photo, id, number,  name);
                contactsList.add(item);
            }
            contactsCursor.close();

        }

        return contactsList;
    }


    public ContactItem getContactData(int position) {

        return myDataSet.get(position);
    }

    public int getCount() {
        return myDataSet.size();
    }

}

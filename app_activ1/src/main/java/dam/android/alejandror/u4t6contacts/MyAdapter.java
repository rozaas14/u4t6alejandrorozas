package dam.android.alejandror.u4t6contacts;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private MyContacts myContacts;


    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tfId;
        TextView tfTlf;
        TextView tfName;
        ImageView imImage;
        ConstraintLayout cLayout;

        public MyViewHolder(View view) {

            super(view);
            this.tfId = view.findViewById(R.id.id);
            this.tfTlf = view.findViewById(R.id.tlf);
            this.tfName = view.findViewById(R.id.name);
            this.imImage = view.findViewById(R.id.image);
            this.cLayout = view.findViewById(R.id.contact);
        }

        public void bind(ContactItem data) {

            this.tfId.setText(data.getId());
            this.tfTlf.setText(data.getNumero());
            this.tfName.setText(data.getNombre());
            if (data.getIdImagen() != null)
                this.imImage.setImageURI(Uri.parse(data.getIdImagen()));

        }

    }

    MyAdapter(MyContacts myContacts) {
        this.myContacts = myContacts;
    }


    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card_layout, parent, false);

        return new MyViewHolder(v);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        viewHolder.bind(myContacts.getContactData(position));
    }

    @Override
    public int getItemCount() {
        return myContacts.getCount();
    }
}
